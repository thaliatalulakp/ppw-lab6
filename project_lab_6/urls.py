"""project_lab_6 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.conf.urls import include, url
from django.urls import path
from django.urls import re_path
from appLab6.views import *
from appLab8.views import *
from appLab9.views import *
from appLab10.views import *
from django.conf import settings
from django.conf.urls.static import static



urlpatterns = [
    path('admin/', admin.site.urls),
    re_path(r'^$', myStatus, name='myStatus'),
    re_path(r'^status$', statusPost, name='statusPost'),
    re_path(r'^page1$', page1, name='page1'),
    re_path(r'^lab8$', lab8, name='lab8'),
    path('lab9/', include('appLab9.urls')),
    re_path(r'^lab10$', lab10, name='lab10'),
    path('valid', valid, name="valid"),
    path('post', post, name="post"),
    url(r'^o/', include('oauth2_provider.urls', namespace='oauth2_provider')),
    url(r'^auth/', include('social_django.urls', namespace='social')),

]
