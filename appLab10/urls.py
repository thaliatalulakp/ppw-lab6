from django.contrib import admin
from django.conf.urls import include
from django.urls import path
from django.urls import re_path
from appLab10.views import *


urlpatterns = [
    path('admin/', admin.site.urls),
    path('/valid', valid, name="valid"),
    path('/post', post, name="post"),
]
