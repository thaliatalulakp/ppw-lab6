from django.contrib import admin
from django.conf.urls import include, url
from django.urls import path
from django.urls import re_path
from .views import lab9
from .views import data
from .views import login
from .views import logoutPage
from .views import tambah
from .views import kurang


urlpatterns = [
    path('data/', data, name='data'),
    path('', lab9, name='lab9'),
    url(r'^login/$', login, name='login'),
    url(r'^logout/$', logoutPage, name='logout'),
    path('tambah', tambah, name='tambah'),
    path('kurang', kurang, name='kurang'),

    # url(r'^auth/', include('social_django.urls', namespace='social')),
    # url(r'^o/', include('oauth2_provider.urls', namespace='oauth2_provider')),
]