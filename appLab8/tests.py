from django.test import TestCase, LiveServerTestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .views import lab8

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options


# Create your tests here.
class testLab8(TestCase):
	def test_lab_8_url_is_exist(self):
		response = Client().get('/lab8')
		self.assertEqual(response.status_code, 200)

	def test_lab_8_using_to_do_list_template(self):
		response = Client().get('/lab8')
		self.assertTemplateUsed(response, 'story8.html')

	def test_landing_page_is_completed(self):
		request = HttpRequest()
		response = lab8(request)
		html_response = response.content.decode('utf8')
		self.assertIn("Ubah Tema", html_response)

	def test_add_url_exist(self):
		response = Client().get('/lab8')
		self.assertEqual(response.status_code, 200)

class Story8FunctionalTest(LiveServerTestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        service_log_path = "./chromedriver.log"
        service_args = ['--verbose']
        self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        self.selenium.implicitly_wait(25) 
        super(Story8FunctionalTest,self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(Story8FunctionalTest, self).tearDown()

    def test_text_css(self):
    	selenium = self.selenium
    	selenium.get(self.live_server_url)
    	teks = selenium.find_element_by_tag_name('body')
    	ukuran = teks.value_of_css_property('font-size')
    	marginTop = teks.value_of_css_property('margin-top')
    	self.assertEqual(ukuran, '10px')
    	self.assertEqual(marginTop, '0px')