from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.http import HttpResponse

from django.urls import reverse

# Create your views here.

def lab8(request):
    response = {}
    return render(request, 'story8.html', response)