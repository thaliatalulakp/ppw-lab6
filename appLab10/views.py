from django.shortcuts import render
from django.http import HttpResponseRedirect, JsonResponse
from django.http import HttpResponse
from django.urls import reverse
import requests
from django.views.decorators.csrf import csrf_exempt
import urllib.request, json
from .forms import regis_form
from .models import regis_models


# Create your views here.
def lab10(request):
    # response['forms'] = regis_form
    response = {'forms': regis_form}
    return render(request, 'story10.html', response)

def post(request):
    response = {}
    form = regis_form(request.POST or None)
    if(form.is_valid()):
        data = form.cleaned_data
        model = regis_models(email=data['email'], nama=data['nama'], password=data['password'])
        model.save()
        hasil = {'email' : model.email, 'nama' : model.nama, 'password' : model.password}
        return JsonResponse(hasil)

@csrf_exempt
def valid(request):
    email = request.POST.get('email') 
    if (regis_models.objects.filter(email=email)):
        hasil = {
            'not_valid': True
        }
    else:
        hasil = {
            'not_valid': False
        }
    return JsonResponse(hasil)
