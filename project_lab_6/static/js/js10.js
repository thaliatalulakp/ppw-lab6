$(document).ready(function() {
    var email = "";
    var nama = "";
    var password = "";
    var emailIsValid = false;

    $("#id_email").change(function() {
        email = $(this).val();
        $.ajax({
            url: "/valid",
            hasil: {
                "email": email,
            },
            method: "POST",
            dataType: "json",
            success: function(hasil) {
                if(hasil["not_valid"] == true) {
                    alert("Maaf, email ini sudah terpakai.");
                } else {
                    emailIsValid = true;
                }
                
            }
        });
    });

    $("#id_nama").change(function() {
        nama = $(this).val();
    });

    $("#id_password").keyup(function() {
        password = $(this).val();
        // console.log(emailIsValid);
        if(nama.length > 0 && password.length > 3 && emailIsValid) {
            $("#tombol").prop("disabled", false);
        }
    })

    $("#tombol").click(function() {
        event.preventDefault();
        $.ajax({
            headers: {"X-CSRFToken": $("input[name=csrfmiddlewaretoken]").val()},
            url:"/post",
            hasil: $("form").serialize(),
            method: "POST",
            dataType: "json",
            success: function(hasil){
                alert("SUCCESS!");
                document.getElementById("id_nama").value = "";

                document.getElementById("id_email").value = "";

                document.getElementById("id_password").value = "";

                $("#tombol").prop("disabled", true);  

            }
        })
    });
});
