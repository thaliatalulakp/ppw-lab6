from django.shortcuts import render
from django.http import HttpResponseRedirect, JsonResponse
from django.http import HttpResponse
from django.urls import reverse
import requests

import json 
from django.contrib.auth import authenticate, login, logout

# Create your views here.

def lab9(request):
    response = {}
    return render(request, 'story9.html', response)

def data(request):
    jsonInput = requests.get('https://www.googleapis.com/books/v1/volumes?q=quilting').json()
    if request.user.is_authenticated:
        count = request.session.get('counter',0)
        request.session['counter'] = count
        for key, value in request.session.items():
            print('{} => {}'.format(key, value))
    return JsonResponse(jsonInput)

def login(request):
    response = {}
    if request.user.is_authenticated:
       request.session['user'] = request.user.username
       request.session['email'] = request.user.email

       request.session.get('counter', 0)
       print(dict(request.session))
       for key, value in request.session.items():
           print('{} => {}'.format(key, value))
    return render(request, 'login.html', response)


def logoutPage(request):
    request.session.flush()
    logout(request)
    return HttpResponseRedirect('/lab9/')

def tambah(request):
    print(dict(request.session))
    request.session['counter'] = request.session['counter'] + 1
    return HttpResponse(request.session['counter'], content_type = "application/json")

def kurang(request):
    request.session['counter'] = request.session['counter'] - 1
    return HttpResponse(request.session['counter'], content_type = "application/json")



