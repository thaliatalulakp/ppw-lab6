from django import forms

class regis_form(forms.Form):
	email = forms.EmailField(required=True, widget=forms.EmailInput(attrs={'placeholder' : 'masukan email anda', 'id' : 'id_email'}))
	nama = forms.CharField(required=True, widget=forms.TextInput(attrs={'placeholder' : 'masukan nama anda', 'id' : 'id_nama'}))
	password = forms.CharField(required=True, widget=forms.PasswordInput(attrs={'placeholder' : 'masukan password anda', 'id' : 'id_password'}))