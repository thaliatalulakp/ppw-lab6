$(document).ready(function() {
    var email = "";
    var nama = "";
    var password = "";
    var emailIsValid = false;

    $("#id_email").change(function() {
        email = $(this).val();
        $.ajax({
            url: "/valid",
            data: {
                "email": email,
            },
            type: "POST",
            dataType: "json",
            success: function(hasil) {
                if(hasil["not_valid"] == true) {
                    alert("Maaf, email ini sudah terpakai.");
                } else {
                    emailIsValid = true;
                }
                
            }
        });
    });

    $("#id_nama").change(function() {
        nama = $(this).val();
    });

    $("#id_password").keyup(function() {
        password = $(this).val();
        // console.log(emailIsValid);
        console.log(nama.length);
        console.log(emailIsValid);
        console.log(password.length);

        if(nama.length > 0 && password.length > 3 && emailIsValid) {
            $("#tombol").prop("disabled", false);
        }
    })

    $("#tombol").click(function() {
        event.preventDefault();
        $.ajax({
            headers: {"X-CSRFToken": $("input[name=csrfmiddlewaretoken]").val()},
            url:"/post",
            data: $("form").serialize(),
            type: "POST",
            dataType: "json",
            success: function(hasil){
                alert("berhasil yey!");
                document.getElementById("id_nama").value = "";
                document.getElementById("id_email").value = "";
                document.getElementById("id_password").value = "";
                $("#tombol").prop("disabled", true);  

            }
        })
    });
});
